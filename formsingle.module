<?php

/**
 * ATTENTION THEMERS: use this function if you are hard-coding forms into your theme!
 *
 * Initializes and returns a token. This can be called from the theme level to get a token for a form
 * if your form is hardcoded and doesn't call form_alter when being built the very first time.
 *
 * @param string $form_id
 *   This must match the form_id that will be generated when the form is submitted.
 * @return string
 *   A token. If calling this from your theme, put it in an element like this:
 *
 *   <input type="hidden" name="edit[formsingle_token]" id="edit-formsingle_token" value="<?php print formsingle_initialize_token('your_form_id'); ?>" />
 *
 *   This will guarantee that your form doesn't get blocked by the formsingle module.
 */
function formsingle_initialize_token($form_id)  {
  // Make the original token that will later be needed for the form to submit
  $token = md5($form_id. session_id().time());
  $_SESSION['formsingle_tokens'][time()][$token] = $token;
  return $token;
}


function formsingle_init() {
  if (!is_array($_SESSION['formsingle_tokens'])) {
    $_SESSION['formsingle_tokens'] = array();
  }

  // Make sure that no tokens can stay in the session for more than the maximum session lifetime.
  $stale = time() - ini_get('session.gc_maxlifetime');
  foreach (array_keys($_SESSION['formsingle_tokens']) as $time) {
    if ($time < $stale) {
      unset($_SESSION['formsingle_tokens'][$time]);
    }
  }
}

function formsingle_form_alter($form_id, &$form) {

  // To make life simple, we trust people who can access administrative forms
  if (arg(0) == 'admin') {
    return;
  }

  // Form is being built for the very first time so make a record of this form
  if (!isset($form['formsingle_token']) && $_SERVER['REQUEST_METHOD'] == 'GET') {
    $token = formsingle_initialize_token($form_id);
    $form['formsingle_token'] = array(
      '#type' => 'hidden',
      '#value' => $token,
    );
  }
  // If the token is in the form values being posted...
  elseif ($token = $_POST['formsingle_token']) {
    // ...add it to the form (it doesn't persist otherwise)
    $form['formsingle_token'] = array(
      '#type' => 'hidden',
      '#value' => $token,
    );
    // and add an extra submit function (with the token as the parameter).
    $form['#submit'] = array('formsingle_submit' => array($_POST['formsingle_token'])) + (array)$form['#submit'];
  }
  // Otherwise, something is wrong so we boot them back to the page where they started.
  else {
    $path = isset($_REQUEST['q']) ? $_REQUEST['q'] : '';
    drupal_goto($path);
  }
}

function formsingle_submit($form_id, $form, $token) {
  // This is the main blockade to double submissions. If there is no token, or if the token isn't in the session,
  // don't allow the form to submit and send them back from whence they came.
  $path = isset($_REQUEST['q']) ? $_REQUEST['q'] : '';
  if (!$token) {
    drupal_goto($path);
  }

  $found = false;
  // Look through the formsingle tokens in the session.
  foreach($_SESSION['formsingle_tokens'] as $time => $tokens) {
    if ($key = array_search($token, $tokens)) {
      // Found the token
      $found = true;
      // Unset the token
      unset($_SESSION['formsingle_tokens'][$time][$key]);
      // get out of the loop and allow form submission.
      break;
    }
  }
  // If we exit the above foreach loop and it is still not found, don't accept the form
  if (!$found) {
    drupal_goto($path);
  }
}